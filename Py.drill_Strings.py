## Strings

x = "one two three four"

#Print the last 3 characters of `x
print(x[-3:])

#Print the first 10 characters of `x`
print(x[:11])

#Print characetrs 4 through 10 of `x`
print(x[10:15])

#Find the length of `x`
print(len(x))

#Split `x` into its words
print(x.split())

#Capitalize the first character of `x`
y = x.upper()
x = y[:1] + x[1:]
print(x)

#Convert `x` into uppercase
print(x.upper())

'''
x = "one two three four"
y = x
x = "one two three"


After executing the above code, what is the value of `y`?

>>>y
>>>'one two three four'

'''
