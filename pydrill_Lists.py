#--Lists--

x = [3, 1, 2, 4, 1, 2]


#Find the sum of all the elements in `x`
print("sum of x: {}".format(sum(x)))


# Find the length of `x`
print("Lenght of x: {}".format(len(x)))


#Print the last three items of `x`
print("last three items of x: {}".format(x[-3:]))


#Print the first three items of `x`
print("First three items of x: {}".format(x[:4]))


#Sort `x`
x.sort()
print("sorted x: {}".format(x))


#Add another item, `10` to `x`
x.append(10)


# Add another item `11` to `x`
x.append(11)


# Remove the last item from `x`
x.pop()


#How many times does `1` occur in `x`?
x.count(1)


#Check whether the element `10` is present in `x`
10 in x


#returns TRUE if present and FALSE if not.
#-------------------------------------------------------------------------------------------
y = [5, 1, 2, 3]


#Add all the elements of `y` to `x`
x += y
print("Sum of x and y : {}".format(x))

#Create a copy of `x`
copy_of_x = x.copy()
print("Copy of x: {}".format(copy_of_x))

'''Can a list contain elements of different data-types?
	- Yes 

 Which data structure does Python use to implement a list?

'''
