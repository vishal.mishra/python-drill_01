## break, and continue

#Print all the two-digit prime numbers.
def is_not_prime(n):
    divisor= n//2
    while divisor != 1:
        if (n % divisor) == 0:
            return True
        divisor-=1

def is_prime(number):
    return not is_not_prime(number)

print("All Two Digit Primes Numbers:")
for i in range(2,100):
    if is_prime(i):
        print(i)
