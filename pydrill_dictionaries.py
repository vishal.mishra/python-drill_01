## Dictionaries
'''
Read:
* https://docs.python.org/3/tutorial/datastructures.html#dictionaries
* https://conceptcoaster.com/course/python-tutorial/dictionaries/
'''

# Write a function to find the number of occurrences of each word in a string(Don't consider punctuation characters)

def no_of_occurance(string):
    punctuations=",'\"?."
    no_punct_string = ""
    for ch in string:
        if not ch in punctuations:
            no_punct_string+=ch
    unique_string_list=list(set(no_punct_string.split()))
    string_list=list(no_punct_string.split())
    for i in unique_string_list:
        word_count = string_list.count(i)
        print("{} : {}".format(i,word_count))
    if not string_list:
        print("{}")

'''
Test Cases:

Test Case 0:

Empty String

Output: `{}`

Test Case 1

Input:
```
'''
no_of_occurance("Python is an interpreted, high-level, general-purpose programming language.")


'''```

Output:

```
{'Object-oriented': 1,
         'Python': 3,
         'a': 1,
         'an': 1,
         'and': 1,
         'are': 2,
         'available': 1,
         'for': 1,
         'fully': 1,
         'general-purpose': 1,
         'high-level': 1,
         'interpreted': 1,
         'interpreters': 1,
         'is': 2,
         'language': 2,
         'many': 1,
         'multi-paradigm': 1,
         'operating': 1,
         'programming': 4,
         'structured': 1,
         'supported': 1,
         'systems': 1}
```
'''
#Define a function that prints all the `items` (keys, values) in a dictionary


def print_dict_keys_and_values(d):
    # your logic here
    print("\nKey, Value pairs:")
    for i in d:
        print("({},{})".format(i,d[i]))

print_dict_keys_and_values({'a':1,'b':5})

'''
Define a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.

Example:

Input: 

```
['a', 'b', 'c']
```

Output:

```
[('a', 1), ('b', 3), ('c', 2)]
```
'''

