        
## Exceptions

#Modify the above function so that it raises a `FilepathNotValid` exception when you provide a `file_path` that does not exist

def unique(file_path):
    try:
        with open(file_path, 'r') as file:
            file_read = file.read()

        lined_file = file_read.replace(" ", "\n")
        #print(lined_file)
        unique_lines = list(set(list(lined_file.split("\n"))))
        sorted_lines = sorted(unique_lines)
        unique_string = "\n"
        sorted_string = "\n"
        unique_string = unique_string.join(unique_lines)
        sorted_string = sorted_string.join(sorted_lines)
        return sorted_string
    except FileNotFoundError:
        print("FilepathNotValid")

