## for, range and while

#https://conceptcoaster.com/course/python-tutorial/for-and-range/
x = [1, 2, 3, 4]

#Print all the elements in `x`
print("Elements in x:")
for i in x:
    print(i)


#Print all the elements and their indexes using the `enumerate` function
print("\nElement  Index:")
enum = list(enumerate(x))
for a,b in enum:
    print("{} \t {}".format(a,b))


#Print all the integers from 10 to 0 in descending order using `range`
print("\nIntegers from 10 to 0 in reverse order using range():")
for i in range(10,-1,-1):
    print(i)
    
#Print all the integers from 10 to 0 in descending order using `while`
print("\nIntegers from 10 to 0 in reverse order using while:")
i=10
while i>=0:
    print(i)
    i-=1
    
