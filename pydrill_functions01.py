## Functions
'''
 https://conceptcoaster.com/course/python-tutorial/defining-functions-1/
 Write a function that accepts an integer `n`, `n > 0`, and
 returns a `list` of all *n-digit* prime numbers
'''
import pydrill_is_not_prime


def n_digit_primes(n):
    primes = []
    n = 10**n
    for i in range(2,n):
        if not pydrill_is_not_prime.is_not_prime(i):
            primes.append(i)
    return primes


'''
Did you write a helper function? If not, write a helper function, `is_prime`
that returns whether a number is prime or not,and use it in your `n_digit_primes` function
'''
