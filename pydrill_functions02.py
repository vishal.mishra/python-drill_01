
## functions 2

#https://conceptcoaster.com/course/python-tutorial/defining-functions-2/
#To the `n_digit_primes` function, set the default value of argument `n` to 2

import pydrill_is_not_prime


def n_digit_primes(n=2):
    primes = []
    # your logic here
    n = 10**n
    for i in range(2,n):
        if not pydrill_is_not_prime.is_not_prime(i):
            primes.append(i)
    return primes


#Define a function, `args_sum` that accepts an arbitrary number of integers as input and computes their sum.

def arg_sum(*x):
    print(sum(x))


#Modify the args_sum function so that it accepts an optional, boolean, keyword argument named `absolute`. If `absolute` is True, then `args_sum` must return the absolute value of the sum of `*args`. If absolute is not specified, then return the sum without performing any conversion.

def args_sum(*x, **absolute):
    if not absolute['absolute']:
        print(sum(x))
    else:
        summation=0
        for i in x:
            abs_i =abs(i)
            summation+=abs_i
        print(summation)
