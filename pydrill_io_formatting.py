
## IO and String Formatting, File Operations
'''
* https://conceptcoaster.com/course/python-tutorial/io-and-string-formatting/

* Save the first `n` natural numbers and their squares into a file in the csv format.
'''
import csv
def numbers_and_squares(n, file_path):
    with open(file_path, 'w') as csv_file:
        num_list=[]
        
        for i in range(1, n+1):
            nums=[i, i*i]
            num_list.append(nums)

        writer = csv.writer(csv_file)
        print(writer)
        writer.writerows(num_list)

    
'''
Example Output for `n=3`

1,1
2,4
3,9

'''
numbers_and_squares(3, 'file_sample2.csv')
