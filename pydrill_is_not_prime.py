def is_not_prime(n):
    divisor= n//2
    while divisor != 1:
        if (n % divisor) == 0:
            return True
        divisor-=1
