
## Modules
'''
Read https://conceptcoaster.com/course/python-tutorial/modules/
* Organize the code you've written so far into modules.
'''
import pydrill_functions01 as mod

print(mod.n_digit_primes(2))
print(mod.n_digit_primes(1))
