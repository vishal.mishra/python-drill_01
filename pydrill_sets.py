## sets

# https://conceptcoaster.com/course/python-tutorial/sets/
'''

Define a function that does the equivalent of `uniq <file_path> | sort`, i.e., the function should

1. Accept a file path as the argument
2. Read the contents of the file into a string
3. Split the string into lines
4. Remove duplicate lines
5. Sort the lines
6. Return the sorted lines
'''


def unique(file_path):
    with open(file_path, 'r') as file:
        file_read = file.read()

    lined_file= file_read.replace(" ", "\n")
    unique_lines= list(set(list(lined_file.split("\n"))))
    sorted_lines=sorted(unique_lines)
    unique_string=""
    sorted_string=""

    unique_string = unique_string.join(unique_lines)
    sorted_string = sorted_string.join(sorted_lines)
    return sorted_string
